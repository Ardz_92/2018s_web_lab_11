-- Answers to Exercise 7 here
DROP TABLE IF EXISTS ex7_articleTable;

CREATE TABLE IF NOT EXISTS ex7_articleTable (
  comment_id            INT NOT NULL AUTO_INCREMENT,
  article_id INT NOT NULL,
  comments VARCHAR(200),
  PRIMARY KEY (comment_id),
  FOREIGN KEY (article_id) REFERENCES ex4_articleTable (id)
);

INSERT INTO ex7_articleTable (article_id, comments) VALUES (1, 'The funniest thing I have ever read'),
  (1, 'The')