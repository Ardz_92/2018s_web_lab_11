-- Answers to Exercise 2 here
DROP TABLE IF EXISTS ex2_table;

CREATE TABLE IF NOT EXISTS ex2_table (
  username VARCHAR(50),
  first_name VARCHAR(20),
  last_name VARCHAR(20),
  email VARCHAR(50)
);

INSERT INTO ex2_table VALUES ('peter01', 'Peter', 'Peterson', 'peter.peterson@email.nz');
INSERT INTO ex2_table VALUES ('pete01', 'Pete', 'Peterson', 'pete.peterson@email.nz');
INSERT INTO ex2_table VALUES ('craigP01', 'Craig', 'Peterson', 'craig.peterson@email.nz');
INSERT INTO ex2_table VALUES ('peter01', 'Petey', 'Peterson', 'petey.peterson@email.nz');