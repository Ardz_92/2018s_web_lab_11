-- Answers to Exercise 9 here
SELECT *
FROM ex3_movietable;

SELECT name, gender, year_born, joined FROM ex3_movietable;

SELECT article_title FROM ex4_articleTable;

SELECT DISTINCT directed_by FROM ex6_movietable;

SELECT movie_title FROM ex6_movietable WHERE weekly_rate<=2;

SELECT username FROM ex5_table ORDER BY username;

SELECT username FROM ex5_table WHERE first_name LIKE 'Pete%';

SELECT username FROM ex5_table WHERE first_name LIKE 'Pete%' OR last_name LIKE 'Pete%';