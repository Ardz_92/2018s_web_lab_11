-- Answers to Exercise 5 here
DROP TABLE IF EXISTS ex5_table;

CREATE TABLE IF NOT EXISTS ex5_table (
  username   VARCHAR(50),
  first_name VARCHAR(20),
  last_name  VARCHAR(20),
  email      VARCHAR(50),
  PRIMARY KEY (username)
);

INSERT INTO ex5_table VALUES ('peter01', 'Peter', 'Peterson', 'peter.peterson@email.nz');
INSERT INTO ex5_table VALUES ('pete01', 'Pete', 'Petrson', 'pete.peterson@email.nz');
INSERT INTO ex5_table VALUES ('craigP01', 'Craig', 'Peterson', 'craig.peterson@email.nz');
INSERT INTO ex5_table VALUES ('peter02', 'Petey', 'Peterson', 'petey.peterson@email.nz');