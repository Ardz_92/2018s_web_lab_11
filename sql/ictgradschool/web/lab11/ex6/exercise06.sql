-- Answers to Exercise 6 here
DROP TABLE IF EXISTS ex6_movietable;

CREATE TABLE IF NOT EXISTS ex6_movietable (
  barcode     INT NOT NULL,
  movie_title VARCHAR(30),
  directed_by VARCHAR(30),
  weekly_rate INT,
  loaned_by   VARCHAR(30) NOT NULL,
  PRIMARY KEY (barcode),
  FOREIGN KEY (loaned_by) REFERENCES ex3_movietable (name)
);

INSERT INTO ex6_movietable (barcode, movie_title, directed_by, weekly_rate, loaned_by) VALUES
  ('665812', 'King Kong', 'Peter Jackson', 4.00, 'Sonny Bill Williams'),
  ('665813', 'Lord of the Rings', 'Peter Jackson', 2.00, 'Margaret Mahy'),
  ('665814', 'Love Actually', 'Richard Curtis', 6.00, 'Apirana Turupa Ngata'),
  ('665815', 'Boss Baby', 'Michael McCullers', 6.00, 'Bic Runga'),
  ('665816', 'Annabell', 'Gary Dauberman', 2.00, 'Dan Carter')