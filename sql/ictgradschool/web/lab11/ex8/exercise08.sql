-- Answers to Exercise 8 here
DELETE FROM ex6_movietable WHERE barcode='665816';
ALTER TABLE ex6_movietable DROP directed_by;
DROP TABLE ex6_movietable;

UPDATE ex6_movietable
SET movie_title = 'Ding Dong'
WHERE barcode='665812';

UPDATE ex6_movietable
SET weekly_rate = '10'
WHERE barcode='665812';

UPDATE ex6_movietable
SET barcode = '665811'
WHERE barcode='665812'
