-- Answers to Exercise 4 here
DROP TABLE IF EXISTS ex4_articleTable;

CREATE TABLE IF NOT EXISTS ex4_articleTable (
  id            INT NOT NULL,
  Article_Title VARCHAR(30),
  Article_Text  TEXT,
  PRIMARY KEY (id)
);

INSERT INTO ex4_articleTable VALUES (1, 'Cupcake Lorem Ipsum',
                                     'Cupcake ipsum dolor sit amet pastry. Candy jujubes wafer gummies. Cotton candy croissant sesame snaps lemon drops. Caramels sweet sesame snaps I love oat cake bonbon brownie. Cookie muffin pudding I love. Sesame snaps cotton candy gummi bears croissant chocolate cookie bonbon gummies. Chocolate cake bear claw I love I love brownie. Jelly-o cheesecake dragée I love sesame snaps danish chupa chups cupcake. Jelly-o chocolate lollipop candy topping. Sweet roll chocolate bear claw I love. Cotton candy gingerbread muffin cake lollipop tootsie roll dessert cupcake gummi bears. I love donut I love. Pie jelly-o lemon drops jelly beans carrot cake I love biscuit wafer. Apple pie tootsie roll topping. Apple pie pie pastry. I love dragée fruitcake I love I love dragée topping sweet roll lemon drops. Gummies lemon drops brownie bonbon bonbon halvah dragée apple pie gummi bears.');
